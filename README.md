# openEuler软件提醒更新工具项目功能说明书

## 1. 引言

### 1.1 项目背景

​		现代软件技术发展突飞猛进，每天都有软件在陆续更新，频繁的更新就显得十分繁琐，如果通过计算机技术设计一套自动安装升级检测系统，就可以减少IT部门的人力成本，提高一定的效率，节省企业成本。

​		在centos中已经提供了两种方法实现自动更新：dnf自动更新和Cockpit Web控制台，但两种更新方式都有局限性，且不是很灵活，如果不是很熟悉使用方法将会遇到各种问题，不是很友好；在参考了一些系统的软件更新提醒的设计后，我们决定自己设计一个便捷的，友好的软件更新提醒工具。

### 1.2 开发意图

​		我们在参考了centos的两种更新机制后，在openEuler系统中我们针对软件更新提醒功能做了二次开发，让软件提醒更新更为便捷，迅速。

### 1.3 用户群体

该工具面向全体使用 openEuler 系统的用户

### 1.4 参考资料

（1）Centos7的软件更新

（2）Ubuntu的软件更新器

（3）openEuler的官方文档与相关博客

（4）python tkinker 模块的官方文档与相关博客

## 2.软件概述

### 2.1  软件标识

#### 2.1.1 项目名称

项目名称：python编写更新提醒软件

#### 2.1.2  产品标识

产品名称： openEuler软件提醒更新工具

产品简称：软件提醒更新工具

编写语言：Python

版本号：1.03

### 2.2 开发思路

（1）在参考了centos的软件更新的方式后，发现centos上这方面功能相对单一，需要用户手动打开软件，才能对软件进行更新检测，所以我们在openEuler上改进了启动方式，我们的更新提醒工具可以在用户登录后，进行弹窗提醒，并且保留了centos上的启动方式，也可以手动在桌面启动，在工具安装完毕后，会自动在启动器里面创建快捷方式，方便用户点击查看（在没有软件更新的情况下，是没有任何提示的）；另外也参考了Ubuntu系统的软件更新器，在此基础上我们也添加了定时启动的设置，方便用户自定义设置。

（2）在图形化模块的选择上，有Tkinter与QT5两种选择，在二者之间，考虑到QT5模块的学习难度，我们选择了更加轻量化与上手难度更低的Tkinter模块。

（3）在字符界面，我们选择了Linux内置的功能——whiptail，但是由于命令行界面的限制，我们测试了许多方法，最终决定在用户登录时随着系统信息一起显示更新提醒，也可以手动执行命令来查看。

### 2.3 特点

- 支持命令行与桌面系统
- 支持开机登录桌面弹窗
- 支持一键更新与选择更新
- 遵循yum/dnf更新机制
- 采用Tkinter模块，更加轻量
- 支持定时弹窗的设置
- 支持桌面图标应用启动
- 支持yum/dnf仓库正确性检测
- 支持日志存储与管理
- 支持dde和gnome两种桌面
- 支持多用户使用
- 支持x86与ARM架构

## 3. 具体功能

**（1）开机登录桌面系统提醒弹窗界面**

 		功能描述：判断是否有更新软件，如果有则弹窗提醒，点击查看详情则前往更新主界面
![输入图片说明](https://gitee.com/openeuler2020/team-1265757126/raw/master/images/gxtx.png "gxtx.png")

**（2）超级管理员root点击软件时的提醒**

 		功能描述：判断是否为root用户，如果是则进行弹窗警告
![输入图片说明](https://gitee.com/openeuler2020/team-1265757126/raw/master/images/tq.png "tq.png")

**（3）软件更新主界面**

 		功能描述：将所有需要更新的软件展示出来，包括软件简介、软件名称、版本号；按照字母的顺序进行排列，界面包括全选、重新扫描、确定更新、退出功能
![输入图片说明](https://gitee.com/openeuler2020/team-1265757126/raw/master/images/zym.png "zym.png")

**（4）更新提醒设置界面**

 		功能描述：目前只有定时弹窗设置、输入具体时间，在有软件更新时，则可以弹窗提醒
![输入图片说明](https://gitee.com/openeuler2020/team-1265757126/raw/master/images/sz.png "sz.png")

**（5）更新完成后提示界面**

 		功能描述：软件更新时会有进度条提示，更新完成后会有弹窗提示
![输入图片说明](https://gitee.com/openeuler2020/team-1265757126/raw/master/images/gxwc.png "gxwc.png")

**（6）命令行更新提醒界面**

 		功能描述：目前只有提醒功能，在有软件更新时，可以看到具体需要更新的软件
![输入图片说明](https://gitee.com/openeuler2020/team-1265757126/raw/master/images/cmdjm.png "cmdjm.png")

## 4. 运行环境

硬件配置：暂无 

架构：x86，ARM

系统：openEuler20.03 LTS SP1

软件配置：Python3（推荐 python3.6 以上）

桌面：DDE与gnome桌面

## 5. 使用说明

### 5.1 安装与初始化

**（1）下载openeuler软件更新工具到任意文件夹并解压（请不要放在 /root下）[（下载链接）](https://gitee.com/openeuler2020/team-1265757126/blob/master/softup_remind-1.0.2.tar.gz)**

```bash
wget -P /usr/  https://gitee.com/openeuler2020/team-1265757126/raw/master/softup_remind-1.0.3.tar.gz

tar zxvf softup_remind-1.0.3.tar.gz
```

**（2）进入openeuler软件更新工具的文件夹**

~~~bash
cd softup_remind
~~~

**（3）添加其他用户的使用权限**

添加用户到wheel 组：usermod -aG wheel username（用户名）

查看wheel组成员：groupmems -g wheel -l

**（4）运行 configure.sh 与 enable.sh 脚本**

~~~bash
bash configure.sh 

bash enable.sh
~~~

**（5）建议关闭 selinux**

**（6）建议安装完成后重启一次系统**

### 5.2 用户使用流程图

![输入图片说明](https://gitee.com/openeuler2020/team-1265757126/raw/master/images/cz.png "cz.png")

### 5.3 软件架构图

![输入图片说明](https://gitee.com/openeuler2020/team-1265757126/raw/master/images/rjjgt.png "rjjgt.png")

架构图分析：

底层使用x86或arm架构，在架构之上运行openEuler，我们对工具权限严格划分，只有管理员（root或者wheel组里的用户）能使用，使用python里的Tkinter模块制作图形界面，shell(linux命令)配合完成业务需求，方便管理员管理，使得工具更加轻量、实用、人性化。

### 5.4 软件使用说明

#### 5.2.1 更新提醒弹窗说明

（1）如果系统有需要更新的软件，我们的工具则会进行弹窗，反之则不弹

（2）更新完成后会有弹窗进行提醒，如果在更新过程中退出，则会中断下载

（3）软件更新提醒工具默认提醒是登录桌面系统后进行弹出（用户注销后登录也可以弹出）

#### 5.2.2 定时更新提醒说明

如果需要定时弹出功能，设置里面有定时弹窗的设置，提供了每天与每一周具体时间的设置（如每天12:00；每一周星期一12:00）

![输入图片说明](https://gitee.com/openeuler2020/team-1265757126/raw/master/images/sz.png "sz.png")

#### 5.2.3 其他用户授权使用说明

如果需要给某个用户单独授权，请将这个用户添加到 wheel组里面，然后重新执行 enable.sh 脚本

#### 5.2.4 命令行更新提醒说明

在登录时可以随着系统信息一起显示更新提醒，还可以执行命令—— `cmd_remind`来查看更新提醒。

#### 5.2.5 桌面快捷方式添加说明

更新提醒工具默认将软件添加到桌面的启动器里，如果需要添加到桌面上，可以进行手动添加

![输入图片说明](https://gitee.com/openeuler2020/team-1265757126/raw/master/images/zmtb.png "zmtb.png")

## 6. 代码文件功能介绍

### 6.2 代码架构图

![输入图片说明](https://gitee.com/openeuler2020/team-1265757126/raw/master/images/dmjgt.png "dmjgt.png")

### 6.2 代码文件功能介绍

**（1）bin/main.py —— 主程序执行文件**

详情介绍：包含更新提醒的主界面，图形界面采用Tkinter模块编写

**（2）core/update_list.py —— 获取可更新软件的名称与详情**

详情介绍：可更新软件名称的获取

softname函数：使用os模块调用yum list yodates获取可更新的软件列表 再通过正则匹配出软件的名字，如果没有软件可更新则返回空 有软件更新则返回软件名

softlist函数：通过已经过滤出的软件名使用os模块调用yum info以及grep，sed，awk过滤出软件的简介，返回软件的描述信息

**（3）core/update_win.py ——  软件更新界面，包括软件选择，更新确定等** 

详情介绍：软件提醒的主界面，图形界面采用Tkinter模块编写，功能包含：更新软件展示，全选与多选，更新设置等等

**（4）core/warning.py —— root用户运行警告**

详情介绍：参考centos系统的软件更新设置，判断当前用户的uid，uid为0 则为root用户，则弹出警告窗口

**（5）core/setup.py —— 更新设置界面**

详情介绍：主要是设置定时提醒的设置，用户可以手动设置定时提醒的时间（没有更新时则不会退出）

**（6）core/crontabmodify.py —— 定时启动配置脚本**

详情介绍：使用 export DISPLAY={} 的方式写计划任务，确保能够在图形化界面中能够弹窗

~~~python
  num=str(os.popen("echo $DISPLAY").read().strip())  #保证能够在图形界面中执行
    if (week == 0):
        fund = "echo {} {} \* \* \*  export DISPLAY={} \&\& python3 $SOFTUP_HOME/bin/main.py |sudo tee -a /var/spool/cron/{} >>/dev/null".format(
            minute, hours,num ,user.strip())  
        os.system(fund)
~~~

**（7）core/logs.py —— 日志记录脚本**

详情介绍：利用Python内置模块 logging 将软件更新成功或是失败以及提醒工具的启动按照设定的格式输入到当前目录的.log文件中

使用了logging.basicConfig 来进行日志文件的格式化输入

**（8）core/openEuler_update.log —— 日志记录文件**

详情介绍：记录更新提醒工具的启动情况与软件更新情况

**（9）configure.sh —— 用户权限添加配置脚本**

详情介绍：加添（wheel组成员）软件登录弹窗功能，
使用/etc/xdg/autostart/ 来实现开机登录弹窗，
使用文件的权限来限制非指定用户能够弹窗。

~~~bash
echo "[Desktop Entry]
Type=Application
Exec=python3 $softup_remind/bin/main.py
" > /etc/xdg/autostart/softup_remind.desktop
~~~

**（10）enable.sh —— 确保wheel组成员能使用crontab设置**

详情介绍：确保指定用户拥有定时提醒的权限（计划任务文件的权限）与字符界面登录提醒

**（11）bin/cmd_remind —— 命令行提醒脚本**

详情介绍：采用Linux中whiptail编写。

**（12）bin/softup_remind.desktop —— 软件生成桌面图标文件**

详情介绍：桌面创建软件快捷方式模板

## 7. 常见问题

（1）在初次安装后，第一次登录弹窗可能会等待一段时间后才会弹出

（2）在对yum仓库检测时，时间会有点长


## 8. 日志查看与管理

### 8.1 日志查看 

更新提醒工具在运行时会产生两种日志，业务日志和操作日志。

1、业务日志：

路径：softup_remind/logs/openEuler_update.log

功能：软件更新的历史记录

2、操作日志：

路径：softup_remind/logs/openEuler_update.log

功能：记录更新工具启动情况

### 8.2 日志管理

当日志文件内容达到20行后，会自动备份一份名称后缀为当前时间的备份文件 