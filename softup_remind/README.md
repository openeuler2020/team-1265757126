# openeuler软件更新工具


#### 更新提醒工具使用说明：

（1）如果系统有需要更新的软件，我们的工具则会进行弹窗，如果没有则不弹

（2）在更新界面如果选择了全选，点击"确定更新"按钮，后台则是直接通过yum的全部更新命令进行更新，如果是部分选择，则是利用yum的机制，对所选择的软件一个个进行更新

（3）更新完成后会有弹窗进行提醒，如果在更新过程中退出，则会中断下载

（4）软件更新提醒工具默认提醒是登录桌面系统后进行弹出（用户注销后登录也可以弹出）

（5）如果需要定时弹出功能，设置里面有相关的设置，提供了每天与每一周具体时间的的方案

（6）如果需要给某个用户单独授权，请将这个用户给予sudo权限（设置为 NOPASSWD: ALL ）并添加到 wheel （将wheel组也配置为 NOPASSWD: ALL ）组里面，然后重新执行 enable.sh 脚本

#### 更新提醒工具操作方法：

**（1）下载openeuler软件更新工具到任意文件夹并解压（请不要放在 /root下）[（下载链接）](https://gitee.com/openeuler2020/team-1265757126/blob/master/softup_remind-1.0.2.tar.gz)**

```bash
wget -P /usr/  https://gitee.com/openeuler2020/team-1265757126/raw/master/softup_remind-1.0.0.tar.gz

tar zxvf softup_remind-1.0.0.tar.gz
```

**（2）进入openeuler软件更新工具的文件夹**

~~~bash
cd softup_remind
~~~

**（3）添加其他用户的使用权限**

添加用户到wheel 组：usermod -aG wheel username（用户名）

查看wheel组成员：groupmems -g wheel -l

**（4）运行 configure.sh 与 enable.sh 脚本**

~~~bash
bash configure.sh 

bash enable.sh
~~~

**（5）建议关闭 selinux**

**（6）建议安装完成后重启一次系统**

#### 脚本文件介绍：

**main.py —— 主程序执行文件**

**update_list.py —— 需要更新软件的列表与详情获取**

**update_win.py ——  软件更新界面，包括软件选择，更新确定等** 

**warning.py —— root用户运行警告**

**setup.py —— 更新设置界面**

**passwd_win.py —— sudo提权用户输入密码脚本**

**configure.sh —— 软件环境配置脚本**

**logs.sh —— 日志记录脚本**

**enable.sh —— 确保wheel组成员能使用crontab设置**

**cmd_remind —— 命令行提醒脚本**

**softup_remind.desktop —— 软件生成桌面图标文件**

**crontabmodify.py —— 定时启动配置脚本**


#### 实现功能：

1.更新软件名称与简介的获取与展示

2.一键更新与选择更新

3.开机指定用户登录之后启动弹窗

4.超级管理员使用软件时的提示

5.设置界面的功能

6.更新通知推送至通知栏

7.更新软件完成时弹窗提醒

8.更新错误时错误弹窗提醒

9.重新扫描刷新功能

10.更新进度条提醒

11.日志记录与管理功能

12.命令行界面软件更新提醒

13.对yum/dnf仓库配置正确的检测


