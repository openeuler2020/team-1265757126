#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os


# 删除功能
def dele(week, weeks, hours, minute, user):
    """
    week:每一周 每一天
    weeks:星期几
    Hours：小时
    Minute：分钟
    """
    crons = ''
    if week == 0:
        crons = f'{minute} {hours} * * *'
    if week == 1:
        crons = f'{minute} {hours} * * {weeks}'
    #获取所有的和本软件相关内容
    fonds = "sudo cat /var/spool/cron/%s | grep  $SOFTUP_HOME/bin/main.py$ | awk -F : '{print $1}'"%(user.strip())
    pathlist = list(os.popen(fonds).read().splitlines())
    #获取到所有的本软件行号
    fonds = "sudo cat /var/spool/cron/%s | grep -n $SOFTUP_HOME/bin/main.py$ | awk -F : '{print $1}'"%(user.strip())
    pathnu = list(os.popen(fonds).read().splitlines())
    #将传入的值与列表中的对比，获取
    for i in range(len(pathnu)):
        #程序返回值对比，当出现相同的就执行删除 i代表的是获取到的列表值
        if crons in pathlist[i]:
            deletes = "sudo sed -i '{}d ' /var/spool/cron/{} ".format(pathnu[i],user.strip())
            os.popen(deletes).read()  # 干掉之前的行
            break 

# 增加
def add(week, weeks, hours, minute, user):
    os.system('source /etc/profile')
    # 增加新的计划任务
    num=str(os.popen("echo $DISPLAY").read().strip())#保证能够在图形界面中执行
    if (week == 0):
        fund = "echo {} {} \* \* \*  export DISPLAY={} \&\& python3 $SOFTUP_HOME/bin/main.py |sudo tee -a /var/spool/cron/{} >>/dev/null".format(
            minute, hours,num ,user.strip())  
        os.system(fund)
    if (week == 1):
        fund = "echo {} {} \* \* {}  export DISPLAY={} \&\& python3 $SOFTUP_HOME/bin/main.py |sudo tee -a /var/spool/cron/{} >>/dev/null".format(
            minute, hours, weeks,num, user.strip())  
        os.system(fund)


# 返回存在的
def exist():
    week_dict = {'1': '星期一', '2': '星期二', '3': '星期三',
                 '4': '星期四', '5': '星期五', '6': '星期六', '7': '星期日'}
    user = os.popen('whoami').read()  # 获取当前用户
    fonds = "sudo cat '/var/spool/cron/%s' | grep  $SOFTUP_HOME/bin/main.py$ " % (user.strip())
    pathnus = list(os.popen(fonds).read().splitlines())
    dates = []
    for string in pathnus:
        fond1 = " echo \'%s \' | awk '{print $1}'" % (string)
        minute = str(os.popen(fond1).read().strip())
        fond2 = " echo \'%s\' | awk '{print $2}'" % (string)
        hours = str(os.popen(fond2).read().strip())
        fond5 = " echo \'%s\' | awk '{print $5}'" % (string)
        weeks = str(os.popen(fond5).read().strip())
        if (weeks == "*"):
            dates.append(f"每一天{hours}时{minute}分")
        else:
            dates.append(f"每一周{week_dict.get(weeks)}{hours}时{minute}分")
    return dates


def main(week, weeks, hours, minute, mode):
    """
    week:0~1判断 每天,每一周
    weeks:0~7 周几 0:无
    hours:0~23 小时
    minute:0~59 分钟
    mode:dele,add 删除,添加
    """
    # 获取当前的用户
    user = os.popen('whoami').read()  # 获取当前用户
    # 获取代码在多少行

    if (mode == 'dele'):
        dele(week, weeks, hours, minute, user)  # dele不用管变量
    elif (mode == 'add'):
        add(week, weeks, hours, minute, user)
