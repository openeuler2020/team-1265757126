#!/usr/bin/python3
# -*- coding: utf-8 -*-

import tkinter as tk


# 点击按钮前往全部更新界面
def start_up():
    win.destroy()
    import core.update_win


# 取消(退出)
def cancel():
    exit()


win = tk.Tk()
win.title("")
# 将窗口出现位置固定在屏幕中间
screenwidth = win.winfo_screenwidth()
screenheight = win.winfo_screenheight()
win.geometry(f"380x150+{int((screenwidth - 620) / 2)}+{int((screenheight - 350) / 2)}")
title1 = tk.Label(win, text='软件更新程序 正在以特权用户身份运行',
                  font=(1), width=35, height=2)
title1.place(x=40, y=0)
title2 = tk.Label(win, text='软件包管理器对安全性是敏感的,出于安全原因,'
                            '\n应避免以特权用户身份运行图形界面应用程序。',
                  width=50, height=2, justify="left")
title2.place(x=-1, y=40)


remind_later = tk.Button(win, text='取消(C)', command=cancel)
remind_later.place(x=50, y=100)

see = tk.Button(win, text='确认继续(A)', command=start_up)
see.place(x=250, y=100)

win.resizable(False, False)
win.mainloop()
