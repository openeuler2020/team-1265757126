#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os
import re


# 获取需要更新的软件名称
def softname():
    update_names = []
    olist = os.popen("yum list updates").readlines() 
    if len(olist) > 2:
        olist = olist[2:]
        for i in olist:
            if len(re.split("\s+", i)) == 2:
                glist = re.split("\s+", i)[0] + " " + re.split("\s+", olist[olist.index(i) + 1])[1]
                olist.pop(olist.index(i) + 1)
                update_names.append(glist)
            else:
                glists = re.split("\s+", i)[0] + " " + re.split("\s+", i)[1]
                update_names.append(glists)
        return update_names
    else:
        return ''


# 获取需要更新的软件列表
def softlist():
    update_list = []
    for update_name in softname():
        os.environ['updatename'] = str(update_name)
        u = os.popen("echo $updatename;yum info $updatename |grep Summary |sed -n '1p' |awk -F ':' '{print $2}'").read().strip()
        update_list.append(u)
    return update_list