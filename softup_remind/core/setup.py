#!/usr/bin/python3
# -*- coding: utf-8 -*-

import re
import tkinter as tk
import crontabmodify as cront
from tkinter import ttk
from tkinter import messagebox
from tkinter.scrolledtext import ScrolledText


# 获取用户设置的时间
# 将时间转换为定时任务
def get_time():
    # 对用户输入做出限制，错误则弹窗 24制  (需要优化)
    # week.get(): 星期
    # hour_var.get(): 小时
    # minute_var.get() 分钟
    global time_rules
    try:
        if 0 <= int(hour_var.get()) < 24:
            try:
                if 0 <= int(minute_var.get()) < 60:
                    if weeks.get() == '每一天':
                        cront.main(0, 0, hour_var.get(), minute_var.get(), 'add')
                        times = f'{weeks.get()}{hour_var.get()}时{minute_var.get()}分'
                        time_rules.append(times)

                    if weeks.get() == '每一周':
                        print(week.get())
                        if week.get() == '':
                            messagebox.showwarning('警告', '请输入星期几')
                        else:
                            cront.main(1, week_dict.get(week.get()), hour_var.get(), minute_var.get(), 'add')
                            times = f'{weeks.get()}{week.get()}{hour_var.get()}时{minute_var.get()}分'
                            time_rules.append(times)

                    rules_choose()
                else:
                    messagebox.showwarning('警告', '请输入整数\n范围:0~60')
            except:
                messagebox.showwarning('警告', '请输入整数\n范围:0~60')
        else:
            messagebox.showwarning('警告', '请输入整数\n范围:0~23')
    except:
        messagebox.showwarning('警告', '请输入整数\n范围:0~23')


# 刷新设置的规则
def refresh():
    global time_rules, v
    time_rules = cront.exist()
    v.clear()
    rules_choose()


# 删除设置的规则
def delete():
    global time_rules
    for i in v:
        nums = re.findall(r"\d+\.?", i.get())
        if i.get()[:3] == '每一天':
            cront.main(0, 0, nums[0], nums[1], 'dele')
        if i.get()[:3] == '每一周':
            cront.main(1, week_dict.get(i.get()[3:6]), nums[0], nums[1], 'dele')
    refresh()



# 0, 0, 0, 1 每一天，星期几，时，分钟
win = tk.Tk()
win.title("更新设置")
win.geometry("410x260")
style = ttk.Style(win)
style.configure('lefttab.TNotebook', tabposition='wn')

# 选项卡
notebook = ttk.Notebook(win, width=340, height=236,
                        style='lefttab.TNotebook')
tab1 = tk.Frame(notebook)
tab2 = tk.Frame(notebook)
notebook.add(tab1, text='更新提醒\n时间设置\n')
notebook.add(tab2, text='更多功能\n后续开发\n中\n')
notebook.place(x=2, y=10)

# 提醒输入
window_text = tk.Label(tab1, text='请输入需要提醒的时间(24时)',
                       font=('Arail', 12))
window_text.place(x=55, y=30)

# 冒号
window_symbol = tk.Label(tab1, text=':', font=('Arail', 12))
window_symbol.place(x=195, y=65)

# 每天/每周
weeks = ttk.Combobox(tab1, width=5)
weeks['value'] = ('', '每一天', '每一周')
weeks.current(0)
weeks.place(x=15, y=65)

# 星期
week = ttk.Combobox(tab1, width=5)
week['value'] = ('', '星期一', '星期二', '星期三', '星期四', '星期五', '星期六', '星期日')
week.current(0)
week.place(x=80, y=65)

# 取出输入的小时数
hour_var = tk.StringVar()
hour_var.set('0')
window_hour = tk.Entry(tab1, textvariable=hour_var, width=5, show=None)
window_hour.place(x=150, y=65)

# 取出输入的分钟数
minute_var = tk.StringVar()
minute_var.set('0')
window_minute = tk.Entry(tab1, textvariable=minute_var, width=5, show=None)
window_minute.place(x=210, y=65)

# '添加'按钮
window_button = tk.Button(tab1, text='添加', width=6, command=get_time)
window_button.place(x=260, y=60)

# '删除'按钮
del_button = tk.Button(tab1, text='删除', width=6, command=delete)
del_button.place(x=260, y=130)

# '刷新'按钮
ref_button = tk.Button(tab1, text='刷新', width=6, command=refresh)
ref_button.place(x=260, y=190)

# 已设置的规则
rule = tk.Label(tab1, text='已设置的规则', font=('Arail', 12))
rule.place(x=95, y=95)

# 输出时间
output_time = tk.Listbox(tab1, width=33, height=6)
output_time.place(x=15, y=120)

text = ScrolledText(output_time, width=27, height=6)
text.grid()

v = []
time_rules = cront.exist()
week_dict = {'星期一': 1, '星期二': 2, '星期三': 3,
             '星期四': 4, '星期五': 5, '星期六': 6, '星期日': 7}


def rules_choose():
    text.delete(1.0, "end")
    for index, item in enumerate(time_rules):
        v.append(tk.StringVar())
        choice = tk.Checkbutton(text, variable=v[-1], text=item,
                                onvalue=item, bg='white', justify='left',
                                anchor='w', offvalue='', )
        text.window_create('end', window=choice)
        text.insert('end', '\n')

rules_choose()

win.resizable(False, False)  # 不允许改变窗口大小
win.mainloop()