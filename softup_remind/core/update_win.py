#!/usr/bin/python3
# -*- coding: utf-8 -*-

import re
import os
import tkinter as tk
import tkinter.messagebox
import core.update_list as ut
import logs.logs as logs
import threading
from tkinter import ttk
from tkinter.scrolledtext import ScrolledText


# 更新设置-点击后前往更新设置界面
def settings():
    softup_home = os.path.dirname(os.path.realpath(__file__))
    os.system(f'python3 {softup_home}/setup.py')


# 稍后提醒(退出)
def cancel():
    exit()


# 重新扫描(获取软件信息)
# 点击后重新对需要更新的软件进行扫描
def again():
    # 软件列表
    global software_list, v
    software_list = ut.softlist()
    text.delete(1.0, "end")
    v.clear()
    software_show()


# 确定更新,获取最终更新软件列表，然后确定更新
def determine():
    def thread_progress():
        progress_bar.start()

    def thread_update():
        softwares = [i.get() for i in v if i.get()]  # softwares：最终更新软件列表
        if softwares == ut.softlist():
            os.system("sudo yum -y update --nobest")
            for software in softwares:
                logs.successful(software)  # 输出更新成功日志
                progress_bar.stop()
            tkinter.messagebox.showinfo('更新提示', f'全部更新完成')
            opt.set('1')
            
        else:
            count = 0
            for software in softwares:
                # 将要下载的软件列表传入linux变量updatename 中
                if os.popen("echo $DESKTOP_SESSION").read().strip() == 'gnome':  # 判断桌面系统
                    software = repr(software)
                    software = re.findall("([^']*?) \s*",software)[0].strip()
                else:
                    software = re.findall(".*\s", software)[0].strip()
                os.environ['updatename'] = str(software)
                os.system("sudo yum -y update $updatename")
                state = os.popen("echo $?").read().strip()
                if state == '0':
                    count += 1
                    logs.successful(software)  # 输出更新成功日志
                else:
                    tkinter.messagebox.showerror('更新提示', f'{software}软件错误')
                    logs.failure(software)  # 输出更新失败日志
            progress_bar.stop()
            tkinter.messagebox.showinfo('更新提示', f'更新完成(共{count}个)')
        again()

    # 使用多线程让软件下载与进度条同时运行
    threads = []
    threads.append(threading.Thread(target=thread_progress))
    threads.append(threading.Thread(target=thread_update))
    for t in threads:
        t.start()


# 反选
def unselectall():
    for index, item in enumerate(software_list):
        v[index].set('')


# 全选
def selectall():
    for index, item in enumerate(software_list):
        if opt.get() == 'ON':
            v[index].set(item)  # 内部代码
        if opt.get() == 'OFF':
            unselectall()


# 窗口初始化
win = tk.Tk()
win.title("软件更新")
# 将窗口出现位置固定在屏幕中间
screenwidth = win.winfo_screenwidth()
screenheight = win.winfo_screenheight()
win.geometry(f"620x380+{int((screenwidth - 620) / 2)}"
             f"+{int((screenheight - 350) / 2)}")

# 更新列表框
list = tk.LabelFrame(win, labelanchor="n")
list.place(relx=0.02, rely=0.15, relwidth=0.96, relheight=0.70)

# 全选
opt = tk.StringVar()
opt.set('1')

select_all = ttk.Checkbutton(list, text='全选', variable=opt,
                             onvalue='ON', offvalue='OFF',
                             command=selectall)
select_all.grid(row=0, column=0, sticky='w')

update_list = tk.Label(list, text='更新列表', width=30, height=1)
update_list.grid(row=0, column=0)

# 针对dde桌面与gnome桌面两个显示大小的不同
if os.popen("echo $DESKTOP_SESSION").read().strip() == 'gnome':
    text = ScrolledText(list, width=82, height=11)
    text.grid()
else:
    text = ScrolledText(list, width=71, height=14)
    text.grid()

v = []

software_list = ut.softlist()


def software_show():
    # 单个软件复选框
    for index, item in enumerate(software_list):
        v.append(tk.StringVar())
        choice = tk.Checkbutton(text, variable=v[-1], text=item,
                                onvalue=item, bg='white', justify='left',
                                anchor='w', offvalue='', )
        text.window_create('end', window=choice)
        text.insert('end', '\n')


software_show()

# 标题
title = tk.Label(win, text='openEuler软件更新工具',
                 font=(12), width=30, height=2)
title.pack()

# 更新进度条
canvas = tk.Canvas(win, width=595, height=5)
progress_bar = ttk.Progressbar(canvas, orient=tk.HORIZONTAL,
                               length=595, mode="determinate")
canvas.create_window(1, 1, anchor=tk.NW, window=progress_bar)
canvas.place(x=10, y=325)

# 所有按钮
settings_button = tk.Button(win, text='更新设置', font=(10),
                            width=9, height=1, command=settings)
settings_button.place(x=10, y=340)

again_button = tk.Button(win, text='重新扫描', font=(10),
                         width=9, height=1, command=again)
again_button.place(x=260, y=340)

cancel_button = tk.Button(win, text='稍后提醒', font=(10),
                          width=9, height=1, command=cancel)
cancel_button.place(x=380, y=340)

determine_button = tk.Button(win, text='确定更新', font=(10),
                             width=9, height=1, command=determine)
determine_button.place(x=500, y=340)

win.resizable(False, False)  # 不允许改变窗口大小

win.mainloop()  # 主窗口循环显示