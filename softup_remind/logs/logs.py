import logging
import os

softup_home=os.path.dirname(os.path.realpath(__file__)) +"/openEuler_update.log"
file_handler = logging.FileHandler(filename=softup_home, mode='a',encoding='utf-8', )

logging.basicConfig(
    # 时间 -用户名 -报错级别 #调用日志的函数名 错误内容
    format='%(asctime)s -%(name)s -%(levelname)s -%(module)s: %(message)s',
    datefmt='%Y-%m-%d %H:%M:%S %p',
    handlers=[file_handler, ],
    level=logging.INFO
)

os.environ['softup_home'] = str(softup_home)

def wc_count():
    if int(os.popen("wc -l $softup_home | awk '{print $1}'").read().strip()) > 20:
        os.popen("cp $softup_home  $softup_home.`date \"+%Y-%m-%d_%H:%M:%S\"` ")
        os.popen("echo > $softup_home")

def successful(name):
    logging.info(name + '软件更新成功')


def failure(name):
    logging.error(name + '软件更新失败')


def start(state):
    if state == 1:
        logging.info ('提醒更新工具启动')
