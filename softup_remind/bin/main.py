#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os
import sys

Base_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(Base_DIR)
import tkinter as tk
from logs import logs
from core import update_list as ut
from tkinter import messagebox


# 点击按钮前往全部更新界面
def start_up():
    win.destroy()
    uid = os.popen("id -u").read().strip()
    if uid == '0':
        import core.warning
    else:
        import core.update_win


# 稍后提醒(退出)
def cancel():
    exit()


# 判断yum/dnf仓库是否正确
if os.system("yum check-update --bugfix &> /dev/null ") != 0:
    yum = tk.Tk()
    yum.withdraw()
    yum.update()
    messagebox.showerror('错误', 'yum/dnf仓库存在问题！')
    yum.destroy()
    exit()

# 判断需要更新软件的数量,为0则退出
if ut.softname() == '':
    exit()

# 判断命令行界面与桌面系统
wind = os.popen("echo $DISPLAY").read().strip()
if wind == '':
    cmd_remind = os.path.dirname(os.path.realpath(__file__)) + "/cmd_remind"
    os.system(f"bash {cmd_remind}")
    exit()

logs.start(1)  # 将软件启动信息传输到日志
logs.wc_count()
# 推送到系统通知栏中
os.popen(f"/usr/bin/notify-send 有可用软件更新（共{len(ut.softname())}个）")

win = tk.Tk()
win.title("软件更新")
screenWidth = win.winfo_screenwidth()  # 窗口出现指点在右上角
win.geometry(f"300x100+{screenWidth - 300}+0")
title = tk.Label(win, text=f'有可用的软件更新{len(ut.softname())}个',
                 font=(1), width=22, height=1)
title.place(x=60, y=10)

remind_later = tk.Button(win, text='稍后提醒', command=cancel)
remind_later.place(x=50, y=50)

see = tk.Button(win, text='立即查看', command=start_up)
see.place(x=190, y=50)

win.resizable(False, False)
win.mainloop()