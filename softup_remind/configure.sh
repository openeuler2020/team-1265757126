#!/bin/bash
demoFun(){
	echo "export SOFTUP_HOME=$softup_remind" >> /etc/profile #添加环境变量
	echo "export PATH=$PATH:$softup_remind/bin/" >> /etc/profile #字符界面做成命令
    chmod g+x $softup_remind/bin/cmd_remind
    source /etc/profile
    chown -R :wheel $SOFTUP_HOME #将软件附给wheel组
    chmod g+sw  $SOFTUP_HOME/logs/
    echo "%wheel        ALL=(ALL)       NOPASSWD: ALL"  >>/etc/sudoers

}

softup_remind=$(pwd)
echo "[Desktop Entry]
Type=Application
Exec=python3 $softup_remind/bin/main.py
" > /etc/xdg/autostart/softup_remind.desktop #开启自启动
cp -f $softup_remind/bin/softup_remind.desktop /usr/share/applications/ #给予桌面图标

python3 $softup_remind/logs/logs.py  #生成日志文件
chmod g+w  $softup_remind/logs/openEuler_update.log


#自动编写环境变量
cat /etc/profile |grep SOFTUP_HOME >/dev/null
if [ $? -ne 0 ];then
	demoFun
else
        i=$(grep -n SOFTUP_HOME /etc/profile |awk -F : '{print $1}') #删掉原先的变量
        sed -i "$i d" /etc/profile
        n=$(grep -n /softup_remind/bin/$ /etc/profile |awk -F : '{print $1}')
        sed -i "$n d" /etc/profile
	    demoFun
fi


#wheel组成员crontab权限

usermod -aG wheel root

#安装依赖环境
yum -y install python3-devel tk python3-rpm-generators
#离线安装情况，使用本地库安装
if [ $? -ne 0 ];then
	echo "Network download failed, attempting to use local package"
	#判断当前是什么系统
	architecture=$(arch)
	#检查环境
	if [ $architecture = "x86_64" ];then

		rpm -qa |grep -w "tk" &> /dev/null
		if [ $? -ne 0 ];then
			rpm -i "$softup_remind/lib/tk-8.6.10-1.oe1.x86_64.rpm" &> /dev/null
fi
		rpm -qa |grep -w python3-rpm-generators  &> /dev/null
                if [ $? -ne 0 ];then
                        rpm -i "$softup_remind/lib/python3-rpm-generators-9-1.oe1.noarch.rpm" &>/dev/null
		fi

		rpm -qa |grep -w python3-devel &>/dev/null
		if [ $? -ne 0 ];then
                        rpm -i "$softup_remind/lib/python3-devel-3.8.5-1.oe1.x86_64.rpm"  &>/dev/null
		fi
	fi
	#检查环境
	if [ $architecture = "aarch64" ];then

	      	rpm -qa |grep -w tk &>/dev/null
                if [ $? -ne 0  ];then
                        rpm -i "$softup_remind/lib/tk-8.6.8-4.oe1.aarch64.rpm" &> /dev/null
                fi

	      	rpm -qa |grep -w python3-rpm-generators  &>/dev/null
                if [ $? -ne 0  ];then
                        rpm -i "$softup_remind/lib/python3-rpm-generators-9-1.oe1.noarch.rpm" &>/dev/null
                fi

	      	rpm -qa |grep -w python3-devel &>/dev/null
                if [ $? -ne 0  ];then
                        rpm -i "$softup_remind/lib/python3-devel-3.7.4-8.oe1.aarch64.rpm" &>/dev/null
                fi
        fi

fi

